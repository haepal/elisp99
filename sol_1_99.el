;; elisp solutions to problems in the following url:
;;
;;     https://wiki.haskell.org/99_questions/1_to_10
;;



;; 1

(defun el99/my-last (xs)
  "Find the last element of a list."
  (let ((y (car xs))
	(ys (cdr xs)))
    (if (null ys) y (my-last ys))))


;; 2

(defun el99/my-but-last (xs)
  "Find the last but one element of a list."
  (let ((helper (lambda (x xs)
		  (let ((y (car xs))
			(ys (cdr xs)))
		    (if (null ys) x (funcall helper y ys)))))
	(y (car xs))
	(ys (cdr xs)))
    (funcall helper y ys)))


;; 3

(defun el99/element-at (xs i)
  "Find the K'th element of a list."
  (cond
   ((eq xs ()) nil)
   ((> i 1) (element-at (cdr xs) (- i 1)))
   ((= i 1) (car xs))
   (t nil)))


;; 4

(defun el99/my-length (xs)
  "Find the number of elements of a list."
  (seq-reduce (lambda (z _) (1+ z)) xs 0))


;; 5

(defun el99/my-reverse (xs)
  "Reverse a list."
  (seq-reduce (lambda (z a) (cons a z)) xs ()))


;; 6

(defun el99/is-palindrome (xs)
  "Find out whether a list is a palindrome."
  (let ((ys (el99/my-reverse xs)))
    (equal xs ys)))


;; 7

(defun el99/my-flatten (xs)
  "Flatten a nested list structure."
  (if (eq xs nil) nil
    (let* ((y (car xs))
	   (ys (cdr xs))
	   (as (if (listp y) (el99/my-flatten y) (list y)))
	   (bs (el99/my-flatten ys)))
      (seq-concatenate 'list as bs))))

(defun el99/my-flatten-2 (xs)
  "Flatten a nested list structure."
  (reverse
   (let* ((cons2 (lambda (lst el) (cons el lst)))
	  (flatjoin (lambda (z a) (seq-reduce cons2 (el99/my-flatten-2 a) z)))
	  (process (lambda (z a) (funcall (if (listp a) flatjoin cons2) z a))))
     (seq-reduce process xs nil))))
	       

;; 8

(defun el99/compress (xs)
  "Eliminate consecutive duplicates of list elements. (deep recursion)"
  (if (eq xs nil) nil
    (let* ((x (car xs))
	   (ys (el99/compress (cdr xs)))
	   (y (car ys)))
      (if (equal x y) ys (cons x ys)))))

(defun el99/compress-2 (xs)
  "Eliminate consecutive duplicates of list elements."
  (reverse
   (seq-reduce (lambda (z a) (if (equal a (car z)) z (cons a z)))
	       (cdr xs)
	       (list (car xs)))))
